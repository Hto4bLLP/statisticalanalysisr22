# Setup
```
asetup StatAnalysis,0.0.4
```

# Running

```
python generateConfigs.py --config ../config/HtoDV_base.config --script runCedarJob.sh --mass 55
```

# Prepare

```
source combine.sh
python tree2graph.py -i limits-merged.root -o limit-merged-TGraph.root
hadd -f limits-R21-R22.root limit-merged-TGraph.root ZH_H125_a55a55_4b.root
```

# Plot

```
cd ../../plotcore
source setup.sh
cd ../../scripts
visualize.py -p plots -s samples -o out --log
```
