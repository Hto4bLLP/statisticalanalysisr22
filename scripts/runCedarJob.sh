#! /bin/bash -l
#
#  This is a template for a batch job which you can use to submit 
#
# See also this link on how to submit to batch queues
# https://twiki.atlas-canada.ca/bin/view/AtlasCanada/ATLASLocalRootBase2#Batch_Jobs
#
#
# Environment variables to pass on
export ALRB_CONT_POSTSETUP="export HOME='/home/USER'"
export ALRB_testPath=",,,,,,,,"
export ALRB_CONT_SWTYPE="singularity"
export ALRB_CONT_RUNPAYLOAD="cd /project/ctb-stelzer/jburzyns/HDMI/StatisticalAnalysis/statisticalanalysisr22/run/; asetup StatAnalysis,0.0.4; cd CONFIGDIR/INDEX; trex-fitter ndwfl SCRIPT.config;"
export ALRB_CONT_CMDOPTS="-B /scratch/USER:/scratch/USER -B /project:/project -B /wlcg:/wlcg"
export ALRB_CONT_PRESETUP="hostname -f; date; id -a"
export ALRB_localConfigDir="/home/USER/cedar"
export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(serverurl=http://frontier-atlas.lcg.triumf.ca:3128/ATLAS_frontier)(serverurl=http://ccfrontier.in2p3.fr:23128/ccin2p3-AtlasFrontier)(proxyurl=http://atlasbpfrontier.cern.ch:3127)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)"

# ideally setupATLAS is defined by the site admins.  Just in case ....
alias | \grep -e "setupATLAS" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    typeset  -f setupATLAS > /dev/null 2>&1
    if [ $? -ne 0 ]; then
	function setupATLAS
	{
            if [ -d  /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ]; then
		export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
		source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh $@
		return $?
            else
		\echo "Error: cvmfs atlas repo is unavailable"
		return 64
            fi
	}
    fi
fi

#WORKDIR/../../src/Hto4bLLPAlgorithm/scripts/
# setupATLAS -c <container> which will run and also return the exit code
#  (setupATLAS is source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh)
setupATLAS -c centos7
exit $?
