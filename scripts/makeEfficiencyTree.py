#!/usr/bin/env python

##******************************************
## makeEfficiencyGraph.py
##
## A script to convert the histogram output of TRExFitter
## to TGraph objects that can be fed into PlotCore
##
## EXAMPLE python tree2graph.py -i inputDirectory -o test.root
##******************************************

import argparse
import ROOT
import numpy as np
import glob


ZH_xsec = 0.081656
WH_xsec = 0.45037
lumi    = 139044.84

def main():

    # parse the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputDir', help='Top level directory containing the yield histograms')
    parser.add_argument('-o', '--outputFile', help='Output root file to store the TGraph objects in')
    args = parser.parse_args()

    directories = glob.glob(f'{args.inputDir}/*/')
    directories = sorted(directories, key=lambda x: float(x.split('a55a55_ctau')[-1].strip('/')))

    ZH_1DV_yields = []
    ZH_2DV_yields = []
    ZH_1DV_errors = []
    ZH_2DV_errors = []

    WH_1DV_yields = []
    WH_2DV_yields = []
    WH_1DV_errors = []
    WH_2DV_errors = []

    ctaus = []

    for i,dir in enumerate(directories):
        ctau = float(dir.split('a55a55_ctau')[-1].strip('/'))
        ctaus.append(ctau)
        histFile = glob.glob(f'{dir}/a55a55_ctau{i}_Fit/Histograms/a55a55_ctau*.root')[0]
        tf = ROOT.TFile(histFile)

        ZH_1DV_yields.append(tf.Get('ZH1DV_ZH').GetBinContent(2))
        ZH_1DV_errors.append(tf.Get('ZH1DV_ZH').GetBinError(2))
        ZH_2DV_yields.append(tf.Get('ZH2DV_ZH').GetBinContent(3))
        ZH_2DV_errors.append(tf.Get('ZH2DV_ZH').GetBinError(3))

        WH_1DV_yields.append(tf.Get('WH1DV_WH').GetBinContent(2))
        WH_1DV_errors.append(tf.Get('WH1DV_WH').GetBinError(2))
        WH_2DV_yields.append(tf.Get('WH2DV_WH').GetBinContent(3))
        WH_2DV_errors.append(tf.Get('WH2DV_WH').GetBinError(3))

        tf.Close()

    ZH_1DV_yields_np = np.array(ZH_1DV_yields)/ (ZH_xsec * lumi) 
    ZH_2DV_yields_np = np.array(ZH_2DV_yields)/ (ZH_xsec * lumi)
    ZH_1DV_errors_np = np.array(ZH_1DV_errors)/ (ZH_xsec * lumi)
    ZH_2DV_errors_np = np.array(ZH_2DV_errors)/ (ZH_xsec * lumi)
                                             
    WH_1DV_yields_np = np.array(WH_1DV_yields)/ (WH_xsec * lumi)
    WH_2DV_yields_np = np.array(WH_2DV_yields)/ (WH_xsec * lumi)
    WH_1DV_errors_np = np.array(WH_1DV_errors)/ (WH_xsec * lumi)
    WH_2DV_errors_np = np.array(WH_2DV_errors)/ (WH_xsec * lumi)

    ctaus_np = np.array(ctaus)
    x_err = np.zeros_like(ctaus_np)

    g_ZH_1DV = ROOT.TGraphErrors(ctaus_np.size, ctaus_np, ZH_1DV_yields_np, x_err, ZH_1DV_errors_np)
    g_ZH_2DV = ROOT.TGraphErrors(ctaus_np.size, ctaus_np, ZH_2DV_yields_np, x_err, ZH_2DV_errors_np)
    g_WH_1DV = ROOT.TGraphErrors(ctaus_np.size, ctaus_np, WH_1DV_yields_np, x_err, WH_1DV_errors_np)
    g_WH_2DV = ROOT.TGraphErrors(ctaus_np.size, ctaus_np, WH_2DV_yields_np, x_err, WH_2DV_errors_np)

    f_out = ROOT.TFile(args.outputFile,"RECREATE")

    g_ZH_1DV.SetName("eff_ZH_1DV")
    g_ZH_2DV.SetName("eff_ZH_2DV")
    g_WH_1DV.SetName("eff_WH_1DV")
    g_WH_2DV.SetName("eff_WH_2DV")

    g_ZH_1DV.Write()
    g_ZH_2DV.Write()
    g_WH_1DV.Write()
    g_WH_2DV.Write()

    f_out.Close()
        
if __name__ == "__main__":
    main()

