#!/usr/bin/env python

##******************************************
## runAll.py
##
## A script to generate the TRExFitter configs from the input
## template and run the fits
##
## EXAMPLE python runAll.py --template ../configs/HtoDV_base.config
##******************************************

import argparse
import os
import math
import getpass
import random


# number of jobs to run (1k toys each)
nJobs = 20

def main():

    # parse the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', help='Template TRExFitter config', default='../config/HtoDV_base.config')
    parser.add_argument('--script', help='Template cedar job config', default='/project/ctb-stelzer/jburzyns/HDMI/StatisticalAnalysis/statisticalanalysisr22/scripts/runCedarJob.sh')
    parser.add_argument('--mass', help='Scalar mass', default='55')
    parser.add_argument( "--account", default='def-stelzer', help="batch system acccount of whoever is sponsoring you")
    parser.add_argument( "--reference", default=None, help="reference file to use for upper scan value")
    parser.add_argument("--inputPath", default='/scratch/jburzyns/HDMI/inputFiles_v0/', help='path to the input ntuples')
    args = parser.parse_args()

    # read the config 
    with open(args.config, 'r') as config:
      config_lines = config.read()


    mass  = args.mass

    n_ctau   = 50
    ctau_max = 10
    ctau_min = 0.0001

    kCR = math.log10(ctau_max/ctau_min)
    interval = kCR/n_ctau

    topDir = args.config.split('/')[-1].split('.')[0]

    # determine the ctaus 
    ctaus = [ ctau_min * pow(10,i * interval) for i in range(n_ctau+1) ]

    # loop over the ctaus
    for index, ctau in enumerate(ctaus):

        if index < 10 or index > 40:
            continue

        random.seed(args.mass)
        # make the output directory, complain if it exists
        try: 
            os.makedirs(f'{topDir}/a{mass}a{mass}_ctau{ctau:.6f}') 
        except OSError as error: 
            print('ERROR: Please remove the output directory or choose a different output directory name.') 
            return

        os.chdir(f'{topDir}/a{mass}a{mass}_ctau{ctau:.6f}')
        for i in range(nJobs):

            os.mkdir(f'{i}')
            os.chdir(f'{i}')

            with open(args.script, 'r') as script:
                commands = script.read()

                commands = commands.replace('USER',str(getpass.getuser()))
                commands = commands.replace('CONFIGDIR',f'{topDir}/a{mass}a{mass}_ctau{ctau:.6f}')
                commands = commands.replace('SCRIPT',f'a{mass}a{mass}_ctau{ctau:.6f}')
                commands = commands.replace('INDEX',f'{i}')

            config_lines_tmp = config_lines

            # use the same seed for each ctau value to keep it smooth
            seed = random.randint(0, 1000)

            #scanMax = 0.1 + pow(10,abs(25-index)*.02) - 1
            scanMax = .02 + .01*abs(25-index)**2
            if args.reference:
                import ROOT
                tf = ROOT.TFile(args.reference)
                g_exp = tf.Get("g_exp_upperlimit_pm2")
                scanMax = 3*g_exp.Eval(ctau)

            # replace the template parameters with the run arguments 
            config_lines_tmp = config_lines_tmp.replace('NTUPLEPATH',args.inputPath)
            config_lines_tmp = config_lines_tmp.replace('SCANMAX',str(scanMax))
            config_lines_tmp = config_lines_tmp.replace('MA',str(args.mass))
            config_lines_tmp = config_lines_tmp.replace('CTAUVAL',str(ctau))
            config_lines_tmp = config_lines_tmp.replace('CTAUIDX',str(index))
            config_lines_tmp = config_lines_tmp.replace('SEED',str(seed))

            with open(f'a{mass}a{mass}_ctau{ctau:.6f}.config', 'w') as config:
                config.write(config_lines_tmp)

            with open('tmp.sh', 'w') as script:
                script.write(commands)

            os.system('sbatch --time=1-12:0 --mem=8G  --account={} tmp.sh'.format(args.account))

            os.chdir('../')
        os.chdir('../../')

if __name__ == "__main__":
    main()

