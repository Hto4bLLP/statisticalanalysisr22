#!/usr/bin/env python

##******************************************
## runAll.py
##
## A script to generate the TRExFitter configs from the input
## template and run the fits
##
## EXAMPLE python runAll.py --template ../configs/HtoDV_base.config
##******************************************

import argparse
import os
import math
import getpass
import random
from combineToys import combine_toys

# number of jobs to run (1k toys each)
nJobs = 100

def main():

    # parse the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--topDir', help='top-level directory where the run output is stored')
    parser.add_argument('--mass', help='Scalar mass', default='55')
    args = parser.parse_args()

    mass  = args.mass

    n_ctau   = 50
    ctau_max = 10
    ctau_min = 0.0001

    kCR = math.log10(ctau_max/ctau_min)
    interval = kCR/n_ctau

    topDir = args.topDir

    # determine the ctaus 
    ctaus = [ ctau_min * pow(10,i * interval) for i in range(n_ctau+1) ]

    # loop over the ctaus
    for index, ctau in enumerate(ctaus):
        if index < 36:
            continue
        combine_toys(topDir, nJobs, mass, ctau, index, writeOutput=True)

if __name__ == "__main__":
    main()

