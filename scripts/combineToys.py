import ROOT
import numpy as np
import array

def combine_toys(topDir, nJobs, mass, ctau, ctauIndex, writeOutput=False, outputFile="combined_results.root"):

    limits = {'exp': None, 'p1': None, 'p2': None, 'm1': None, 'm2': None, 'obs': None}
    hypoTestResult = None

    # add results together using HypoTestInverterResult::Add
    for index in range(nJobs):
        fileName = f'{topDir}/a{mass}a{mass}_ctau{ctau:.6f}/{index}/a{mass}a{mass}_ctau{ctauIndex}_Fit/Limits/Toys.root'
        try:
            f = ROOT.TFile(fileName, "READ")
        except OSError:
            continue

        if not f:
            continue
        if not hypoTestResult:
            hypoTestResult = f.Get("result_mu_BR_Haa")
        else:
            hypoTestResult.Add(f.Get("result_mu_BR_Haa"))

    limits['obs'] = hypoTestResult.UpperLimit()
    limits['m2']  = hypoTestResult.GetExpectedUpperLimit(-2)
    limits['m1']  = hypoTestResult.GetExpectedUpperLimit(-1)
    limits['exp'] = hypoTestResult.GetExpectedUpperLimit(0)
    limits['p1']  = hypoTestResult.GetExpectedUpperLimit(1)
    limits['p2']  = hypoTestResult.GetExpectedUpperLimit(2)

    plot = ROOT.RooStats.HypoTestInverterPlot("HTI_Result_Plot","test",hypoTestResult);

    c1 = ROOT.TCanvas("c1")
    c1.SetLogy(False)

    plot.Draw("")
    c1.SaveAs(f"{topDir}/a{mass}a{mass}_ctau{ctau:.6f}/brasilianFlag.pdf")

    ct   = array.array('d', [ctau])
    obs  = array.array('d', [limits['obs']])
    m2   = array.array('d', [limits['m2']])
    m1   = array.array('d', [limits['m1']])
    exp  = array.array('d', [limits['exp']])
    p1   = array.array('d', [limits['p1']])
    p2   = array.array('d', [limits['p2']])
       
    if writeOutput:
        outfile = ROOT.TFile.Open(f"{topDir}/a{mass}a{mass}_ctau{ctau:.6f}/combined_result.root", "RECREATE")
        tree = ROOT.TTree("stats","")

        tree.Branch("ctau", ct, "ctau/D")
        tree.Branch("exp_upperlimit", exp, "exp_upperlimit/D")
        tree.Branch("obs_upperlimit", obs, "obs_upperlimit/D")
        tree.Branch("exp_upperlimit_plus1", p1, "exp_upperlimit_plus1/D") 
        tree.Branch("exp_upperlimit_plus2", p2, "exp_upperlimit_plus2/D")
        tree.Branch("exp_upperlimit_minus1", m1, "exp_upperlimit_minus1/D")
        tree.Branch("exp_upperlimit_minus2", m2, "exp_upperlimit_minus2/D")

        tree.Fill()
        tree.Write()
        hypoTestResult.Write()
        outfile.Close()
    return limits

if __name__ == "__main__":

    topDir = '/project/ctb-stelzer/jburzyns/HDMI/StatisticalAnalysis/statisticalanalysisr22/run/HtoDV_base'
    nJobs = 100

    result = combine_toys(topDir, nJobs, 55, 0.01, 20, writeOutput=True)
    print(result)


