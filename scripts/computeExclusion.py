#!/usr/bin/env python

import argparse
import ROOT
import numpy as np

def main():

    # parse the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputFile', help='Input root file containing the limit tree')
    parser.add_argument('-v', '--val', help='BR val to check')
    args = parser.parse_args()

    tf = ROOT.TFile(args.inputFile)
    limit = tf.Get("g_exp_upperlimit")

    for i in np.arange(1e-3,1+2e-5,1e-5):
        if i == 1.0:
            break
        if limit.Eval(i) > float(args.val) and limit.Eval(i+1e-5) < float(args.val):
            print(i)
        if limit.Eval(i) < float(args.val) and limit.Eval(i+1e-5) > float(args.val):
            print(i)

if __name__=="__main__":
    main()
