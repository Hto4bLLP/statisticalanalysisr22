#!/usr/bin/env python

##******************************************
## tree2graph.py
##
## A script to convert the limit tree output of TRExFitter
## to TGraph objects that can be fed into PlotCore
##
## EXAMPLE python tree2graph.py -i myLimit_BLIND_CL95.root -o test.root
##******************************************

import argparse
import ROOT
import numpy as np

def main():

    # parse the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputFile', help='Input root file containing the limit tree')
    parser.add_argument('-o', '--outputFile', help='Output root file to store the TGraph objects in')
    args = parser.parse_args()

    df = ROOT.RDataFrame("stats", args.inputFile)


    cols = df.AsNumpy(["ctau", 
                       "exp_upperlimit", 
                       "obs_upperlimit", 
                       "exp_upperlimit_plus1", 
                       "exp_upperlimit_plus2", 
                       "exp_upperlimit_minus1", 
                       "exp_upperlimit_minus2"])

    sort_indices = cols["ctau"].argsort()

    ctau = cols["ctau"][sort_indices]

    exp_upperlimit = cols["exp_upperlimit"][sort_indices]

    exp_upperlimit_plus1 = cols["exp_upperlimit_plus1"][sort_indices]
    exp_upperlimit_plus2 = cols["exp_upperlimit_plus2"][sort_indices]
    exp_upperlimit_minus1 = cols["exp_upperlimit_minus1"][sort_indices]
    exp_upperlimit_minus2 = cols["exp_upperlimit_minus2"][sort_indices]

    exp_upperlimit_plus1  = np.subtract(exp_upperlimit_plus1, exp_upperlimit)
    exp_upperlimit_plus2  = np.subtract(exp_upperlimit_plus2, exp_upperlimit)
    exp_upperlimit_minus1 = np.subtract(exp_upperlimit, exp_upperlimit_minus1)
    exp_upperlimit_minus2 = np.subtract(exp_upperlimit, exp_upperlimit_minus2)

    g_exp_upperlimit        = ROOT.TGraph(ctau.size, ctau, exp_upperlimit)

    g_exp_upperlimit.SetName("g_exp_upperlimit")

    x_err = np.zeros_like(ctau)

    g_exp_upperlimit_pm1  = ROOT.TGraphAsymmErrors(ctau.size, ctau, exp_upperlimit, x_err, x_err, exp_upperlimit_minus1, exp_upperlimit_plus1)
    g_exp_upperlimit_pm2  = ROOT.TGraphAsymmErrors(ctau.size, ctau, exp_upperlimit, x_err, x_err, exp_upperlimit_minus2, exp_upperlimit_plus2)

    g_exp_upperlimit_pm1.SetName("g_exp_upperlimit_pm1")
    g_exp_upperlimit_pm2.SetName("g_exp_upperlimit_pm2")


    f_out = ROOT.TFile(args.outputFile,"RECREATE")

    g_exp_upperlimit.Write() 

    g_exp_upperlimit_pm1.Write() 
    g_exp_upperlimit_pm2.Write() 

    f_out.Close()
        
if __name__ == "__main__":
    main()

